# ytb-bot

This code is based on [ytdlbot](https://github.com/tgbot-collection/ytdlbot)


## Run the test env

```bash
# start the test env
docker-compose -f deploy/test/docker-compose.yml up -d

# create the database
docker-compose -f deploy/test/docker-compose.yml exec mysql bash
# then inside that container
mysql -u root -proot -e "CREATE DATABASE ytdl;"

# then exit the container