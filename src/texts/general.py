START_T="""
Welcome to YouTube Download bot. Type /help for more information. 
Backup bot: @benny_2ytdlbot
Join https://t.me/+OGRC8tp9-U9mZDZl for updates.
"""

HELP_T="""
1. If the bot doesn't work, try again or join https://t.me/+OGRC8tp9-U9mZDZl for updates.

2. Source code: https://github.com/tgbot-collection/ytdlbot
"""

ABOUT_T="""
YouTube Downloader by @BennyThink.\n\nOpen source on GitHub: https://github.com/tgbot-collection/ytdlbot
"""

BUY_T="""
**Terms:**
1. You can use this bot to download video for {} times within a 24-hour period.

2. You can buy additional download tokens, valid permanently.

3. Refunds are possible, contact me if you need that @BennyThink

4. Download for paid user will be automatically changed to Local mode to avoid queuing.

5. Paid user can download files larger than 2GB.

**Price:**
valid permanently
1. 1 USD == {} tokens
2. 7 CNY == {} tokens
3. 10 TRX == {} tokens

**Payment options:**
Pay any amount you want. For example you can send 20 TRX for {} tokens.
1. AFDIAN(AliPay, WeChat Pay and PayPal): {}
2. Buy me a coffee: {}
3. Telegram Bot Payment(Stripe), please click Bot Payment button.
4. TRON(TRX), please click TRON(TRX) button.

**After payment:**
1. Afdian: attach order number with /redeem command (e.g., `/redeem 123456`).
2. Buy Me a Coffee: attach email with /redeem command (e.g., `/redeem 123@x.com`). **Use different email each time.**
3. Telegram Payment & Tron(TRX): automatically activated within 60s. Check /start to see your balance.

Want to buy more token with Telegram payment? Let's say 100? Here you go! `/buy 123`
"""

PRIVATE_T = "This bot is for private use"

MEMBERSHIP_REQUIRE_T = "You need to join this group or channel to use this bot\n\nhttps://t.me/{}"

SETTINGS_T = """
Please choose the preferred format and video quality for your video. These settings only **apply to YouTube videos**.

High quality is recommended. Medium quality aims to 720P, while low quality is 480P.

If you choose to send the video as a document, it will not be possible to stream it.

Your current settings:
Video quality: **{0}**
Sending format: **{1}**
"""


PREMIUM_WARNING_T = """
Your file is too big, do you want me to try to send it as premium user? 
This is an experimental feature so you can only use it once per day.
Also, the premium user will know who you are and what you are downloading. 
You may be banned if you abuse this feature.
"""

GET_RECEIVE_LINK_TEXT_T = "Your tasks was added to the reserved queue {}. Processing...\n\n"

PING_WORKER_T = """
{}
"""