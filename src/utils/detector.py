import logging
import os
import contextlib
import inspect as pyinspect

class Detector:
    def __init__(self, logs: str):
        self.logs = logs

    @staticmethod
    def func_name():
        with contextlib.suppress(Exception):
            return pyinspect.stack()[1][3]
        return "N/A"

    def auth_key_detector(self):
        text = "Server sent transport error: 404 (auth key not found)"
        if self.logs.count(text) >= 3:
            logging.critical("auth key not found: %s", self.func_name())
            os.unlink("*.session")
            return True

    def updates_too_long_detector(self):
        # If you're seeing this, that means you have logged more than 10 device
        # and the earliest account was kicked out. Restart the program could get you back in.
        indicators = [
            "types.UpdatesTooLong",
            "Got shutdown from remote",
            "Code is updated",
            "OSError: Connection lost",
            "[Errno -3] Try again",
            "MISCONF",
        ]
        for indicator in indicators:
            if indicator in self.logs:
                logging.critical("kick out crash: %s", self.func_name())
                return True
        logging.debug("No crash detected.")

    def next_salt_detector(self):
        text = "Next salt in"
        if self.logs.count(text) >= 5:
            logging.critical("Next salt crash: %s", self.func_name())
            return True

    def connection_reset_detector(self):
        text = "Send exception: ConnectionResetError Connection lost"
        if self.logs.count(text) >= 5:
            logging.critical("connection lost: %s ", self.func_name())
            return True
