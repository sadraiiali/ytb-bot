#!/usr/local/bin/python3
# coding: utf-8

# ytdlbot - constant.py
# 8/16/21 16:59
#

__author__ = "Benny <benny.think@gmail.com>"

import os

from src.config import (
    AFD_LINK,
    COFFEE_LINK,
    ENABLE_CELERY,
    FREE_DOWNLOAD,
    REQUIRED_MEMBERSHIP,
    TOKEN_PRICE,
    CUSTOM_TEXT,
)
from src.database.influxdb import InfluxDB
from src.texts.general import (
    START_T,
    HELP_T,
    ABOUT_T,
    BUY_T,
    PRIVATE_T,
    MEMBERSHIP_REQUIRE_T,
    SETTINGS_T,
    PREMIUM_WARNING_T,
    GET_RECEIVE_LINK_TEXT_T,
    PING_WORKER_T
)
from src.utils.other import get_func_queue


class BotText:
    start = START_T

    help = HELP_T

    about = ABOUT_T

    buy = BUY_T.format(
        FREE_DOWNLOAD, 
        TOKEN_PRICE, 
        AFD_LINK,
        COFFEE_LINK, 
        TOKEN_PRICE, 
        REQUIRED_MEMBERSHIP,
        REQUIRED_MEMBERSHIP
    )

    private = PRIVATE_T

    membership_require = MEMBERSHIP_REQUIRE_T.format(REQUIRED_MEMBERSHIP)

    settings = SETTINGS_T

    custom_text = CUSTOM_TEXT
    
    premium_warning = PREMIUM_WARNING_T

    @staticmethod
    def get_receive_link_text() -> str:
        reserved = get_func_queue("reserved")
        if ENABLE_CELERY and reserved:
            text = GET_RECEIVE_LINK_TEXT_T.format(reserved)
        else:
            text = "Your task was added to active queue.\nProcessing...\n\n"

        return text

    @staticmethod
    def ping_worker() -> str:
        from tasks import app as celery_app

        workers = InfluxDB().extract_dashboard_data()
        # [{'celery@BennyのMBP': 'abc'}, {'celery@BennyのMBP': 'abc'}]
        response = celery_app.control.broadcast("ping_revision", reply=True)
        revision = {}
        for item in response:
            revision.update(item)

        text = PING_WORKER_T.format(workers, revision)

        return text
