import logging

def customize_logger(logger: list):
    for log in logger:
        logging.getLogger(log).setLevel(level=logging.INFO)
