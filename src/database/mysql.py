import pymysql
from src.config import MYSQL_HOST, MYSQL_PASS, MYSQL_USER, MYSQL_DB_NAME
import logging
from src.database.fake_mysql import FakeMySQL


class MySQL:
    vip_sql = """
    CREATE TABLE if not exists `payment`
    (
        `user_id`        bigint NOT NULL,
        `payment_amount` float        DEFAULT NULL,
        `payment_id`     varchar(256) DEFAULT NULL,
        `old_user`       tinyint(1)   DEFAULT NULL,
        `token`          int          DEFAULT NULL,
        UNIQUE KEY `payment_id` (`payment_id`)
    ) CHARSET = utf8mb4
                """

    settings_sql = """
    create table if not exists  settings
    (
        user_id    bigint          not null,
        resolution varchar(128) null,
        method     varchar(64)  null,
        mode varchar(32) default 'Celery' null,
        history varchar(10) default 'OFF' null,
        constraint settings_pk
            primary key (user_id)
    );
            """

    channel_sql = """
    create table if not exists channel
    (
        link              varchar(256) null,
        title             varchar(256) null,
        description       text         null,
        channel_id        varchar(256),
        playlist          varchar(256) null,
        latest_video varchar(256) null,
        constraint channel_pk
            primary key (channel_id)
    ) CHARSET=utf8mb4;
    """

    subscribe_sql = """
    create table if not exists subscribe
    (
        user_id    bigint       null,
        channel_id varchar(256) null,
        is_valid boolean default 1 null
    ) CHARSET=utf8mb4;
    """
    history_sql = """
    create table if not exists history
    (
        user_id bigint null,
        link varchar(256) null,
        title varchar(512) null
    ) CHARSET=utf8mb4;
    """

    def __init__(self):
        try:
            self.con = pymysql.connect(
                host=MYSQL_HOST,
                user=MYSQL_USER,
                passwd=MYSQL_PASS,
                db=MYSQL_DB_NAME,
                charset="utf8mb4"
            )
            self.con.ping(reconnect=True)
        except Exception:
            logging.warning(
                "MySQL connection failed, using fake mysql instead.")
            self.con = FakeMySQL()

        self.con.ping(reconnect=True)
        self.cur = self.con.cursor()
        self.init_db()
        super().__init__()

    def init_db(self):
        self.cur.execute(self.vip_sql)
        self.cur.execute(self.settings_sql)
        self.cur.execute(self.channel_sql)
        self.cur.execute(self.subscribe_sql)
        self.cur.execute(self.history_sql)
        self.con.commit()

    def __del__(self):
        self.con.close()

    def get_user_settings(self, user_id: int) -> tuple:
        self.cur.execute(
            "SELECT * FROM settings WHERE user_id = %s", (user_id,))
        data = self.cur.fetchone()
        if data is None:
            return 100, "high", "video", "Celery", "OFF"
        return data

    def set_user_settings(self, user_id: int, field: str, value: str):
        cur = self.con.cursor()
        cur.execute("SELECT * FROM settings WHERE user_id = %s", (user_id,))
        data = cur.fetchone()
        if data is None:
            resolution = method = ""
            if field == "resolution":
                method = "video"
                resolution = value
            if field == "method":
                method = value
                resolution = "high"
            cur.execute("INSERT INTO settings VALUES (%s,%s,%s,%s,%s)",
                        (user_id, resolution, method, "Celery", "OFF"))
        else:
            cur.execute(
                f"UPDATE settings SET {field} =%s WHERE user_id = %s", (value, user_id))
        self.con.commit()

    def show_history(self, user_id: int):
        self.cur.execute(
            "SELECT link,title FROM history WHERE user_id = %s", (user_id,))
        data = self.cur.fetchall()
        return "\n".join([f"{i[0]} {i[1]}" for i in data])

    def clear_history(self, user_id: int):
        self.cur.execute("DELETE FROM history WHERE user_id = %s", (user_id,))
        self.con.commit()

    def add_history(self, user_id: int, link: str, title: str):
        self.cur.execute("INSERT INTO history VALUES (%s,%s,%s)",
                         (user_id, link, title))
        self.con.commit()

    def search_history(self, user_id: int, kw: str):
        self.cur.execute(
            "SELECT * FROM history WHERE user_id = %s AND title like %s", (user_id, f"%{kw}%"))
        data = self.cur.fetchall()
        if data:
            return data
        return None
