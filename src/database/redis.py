import logging
from beautifultable import BeautifulTable
from io import BytesIO
import fakeredis
import redis
from src.config import IS_BACKUP_BOT, REDIS
import re
from src.database.mysql import MySQL
import subprocess
import os
import time
import base64
import requests

from src.config import FLOWER_USERNAME, FLOWER_PASSWORD

def get_worker_data() -> dict:
        username = FLOWER_USERNAME
        password = FLOWER_PASSWORD
        token = base64.b64encode(f"{username}:{password}".encode()).decode()
        headers = {"Authorization": f"Basic {token}"}
        r = requests.get(
            "https://celery.dmesg.app/workers?json=1", 
            headers=headers
        )
        if r.status_code != 200:
            return dict(data=[])
        return r.json()

class Redis:
    def __init__(self):
        db = 1 if IS_BACKUP_BOT else 0
        try:
            self.r = redis.StrictRedis(host=REDIS, db=db, decode_responses=True)
            self.r.ping()
        except Exception:
            logging.warning("Redis connection failed, using fake redis instead.")
            self.r = fakeredis.FakeStrictRedis(host=REDIS, db=db, decode_responses=True)

        db_banner = "=" * 20 + "DB data" + "=" * 20
        quota_banner = "=" * 20 + "Celery" + "=" * 20
        metrics_banner = "=" * 20 + "Metrics" + "=" * 20
        usage_banner = "=" * 20 + "Usage" + "=" * 20
        vnstat_banner = "=" * 20 + "vnstat" + "=" * 20
        self.final_text = f"""
{db_banner}
%s


{vnstat_banner}
%s


{quota_banner}
%s


{metrics_banner}
%s


{usage_banner}
%s
        """
        super().__init__()

    def __del__(self):
        self.r.close()

    def update_metrics(self, metrics: str):
        logging.info(f"Setting metrics: {metrics}")
        all_ = f"all_{metrics}"
        today = f"today_{metrics}"
        self.r.hincrby("metrics", all_)
        self.r.hincrby("metrics", today)

    @staticmethod
    def generate_table(header, all_data: list):
        table = BeautifulTable()
        for data in all_data:
            table.rows.append(data)
        table.columns.header = header
        table.rows.header = [str(i) for i in range(1, len(all_data) + 1)]
        return table

    def show_usage(self):
        db = MySQL()
        db.cur.execute("select user_id,payment_amount,old_user,token from payment")
        data = db.cur.fetchall()
        fd = []
        for item in data:
            fd.append([item[0], item[1], item[2], item[3]])
        db_text = self.generate_table(["ID", "pay amount", "old user", "token"], fd)

        fd = []
        hash_keys = self.r.hgetall("metrics")
        for key, value in hash_keys.items():
            if re.findall(r"^today|all", key):
                fd.append([key, value])
        fd.sort(key=lambda x: x[0])
        metrics_text = self.generate_table(["name", "count"], fd)

        fd = []
        for key, value in hash_keys.items():
            if re.findall(r"\d+", key):
                fd.append([key, value])
        fd.sort(key=lambda x: int(x[-1]), reverse=True)
        usage_text = self.generate_table(["UserID", "count"], fd)

        worker_data = get_worker_data()
        fd = []
        for item in worker_data["data"]:
            fd.append(
                [
                    item.get("hostname", 0),
                    item.get("status", 0),
                    item.get("active", 0),
                    item.get("processed", 0),
                    item.get("task-failed", 0),
                    item.get("task-succeeded", 0),
                    ",".join(str(i) for i in item.get("loadavg", [])),
                ]
            )

        worker_text = self.generate_table(
            ["worker name", "status", "active", "processed", "failed", "succeeded", "Load Average"], fd
        )

        # vnstat
        if os.uname().sysname == "Darwin":
            cmd = "/opt/homebrew/bin/vnstat -i en0".split()
        else:
            cmd = "/usr/bin/vnstat -i eth0".split()
        vnstat_text = subprocess.check_output(cmd).decode("u8")
        return self.final_text % (db_text, vnstat_text, worker_text, metrics_text, usage_text)

    def reset_today(self):
        pairs = self.r.hgetall("metrics")
        for k in pairs:
            if k.startswith("today"):
                self.r.hdel("metrics", k)

        self.r.delete("premium")

    def user_count(self, user_id):
        self.r.hincrby("metrics", user_id)

    def generate_file(self):
        text = self.show_usage()
        file = BytesIO()
        file.write(text.encode("u8"))
        date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        file.name = f"{date}.txt"
        return file

    def add_send_cache(self, unique: str, file_id: str):
        self.r.hset("cache", unique, file_id)

    def get_send_cache(self, unique) -> str:
        return self.r.hget("cache", unique)

    def del_send_cache(self, unique):
        return self.r.hdel("cache", unique)


