from influxdb import InfluxDBClient
import os
import base64
import requests
import datetime
import re
import contextlib
import logging

from src.database.redis import Redis
from src.config import (
    INFLUX_HOST,
    INFLUX_PASS,
    INFLUX_PORT,
    INFLUX_USER,
    INFLUX_PATH,
    INFLUX_DB,
    FLOWER_USERNAME,
    FLOWER_PASSWORD
)


class InfluxDB:
    def __init__(self):
        self.client = InfluxDBClient(
            host=INFLUX_HOST,
            path=INFLUX_PATH,
            port=INFLUX_PORT,
            username=INFLUX_USER,
            password=INFLUX_PASS,
            database=INFLUX_DB,
            ssl=True,
            verify_ssl=True,
        )
        self.data = None

    def __del__(self):
        self.client.close()

    @staticmethod
    def get_worker_data() -> dict:
        username = FLOWER_USERNAME
        password = FLOWER_PASSWORD
        token = base64.b64encode(f"{username}:{password}".encode()).decode()
        headers = {"Authorization": f"Basic {token}"}
        r = requests.get(
            "https://celery.dmesg.app/workers?json=1", 
            headers=headers
        )
        if r.status_code != 200:
            return dict(data=[])
        return r.json()

    def extract_dashboard_data(self):
        self.data = self.get_worker_data()
        json_body = []
        for worker in self.data["data"]:
            load1, load5, load15 = worker["loadavg"]
            t = {
                "measurement": "tasks",
                "tags": {
                    "hostname": worker["hostname"],
                },
                "time": datetime.datetime.utcnow(),
                "fields": {
                    "task-received": worker.get("task-received", 0),
                    "task-started": worker.get("task-started", 0),
                    "task-succeeded": worker.get("task-succeeded", 0),
                    "task-failed": worker.get("task-failed", 0),
                    "active": worker.get("active", 0),
                    "status": worker.get("status", False),
                    "load1": load1,
                    "load5": load5,
                    "load15": load15,
                },
            }
            json_body.append(t)
        return json_body

    def __fill_worker_data(self):
        json_body = self.extract_dashboard_data()
        self.client.write_points(json_body)

    def __fill_overall_data(self):
        active = sum([i["active"] for i in self.data["data"]])
        json_body = [{"measurement": "active", "time": datetime.datetime.utcnow(), "fields": {
            "active": active}}]
        self.client.write_points(json_body)

    def __fill_redis_metrics(self):
        json_body = [{"measurement": "metrics",
                      "time": datetime.datetime.utcnow(), "fields": {}}]
        r = Redis().r
        hash_keys = r.hgetall("metrics")
        for key, value in hash_keys.items():
            if re.findall(r"^today", key):
                json_body[0]["fields"][key] = int(value)

        self.client.write_points(json_body)

    def collect_data(self):
        if INFLUX_HOST is None:
            return

        with contextlib.suppress(Exception):
            self.data = self.get_worker_data()
            self.__fill_worker_data()
            self.__fill_overall_data()
            self.__fill_redis_metrics()
            logging.debug("InfluxDB data was collected.")
