import contextlib
import sqlite3
import re

init_con = sqlite3.connect(":memory:", check_same_thread=False)

class Cursor:
    def __init__(self):
        self.con = init_con
        self.cur = self.con.cursor()

    def execute(self, *args, **kwargs):
        sql = self.sub(args[0])
        new_args = (sql,) + args[1:]
        with contextlib.suppress(sqlite3.OperationalError):
            return self.cur.execute(*new_args, **kwargs)

    def fetchall(self):
        return self.cur.fetchall()

    def fetchone(self):
        return self.cur.fetchone()

    @staticmethod
    def sub(sql):
        sql = re.sub(r"CHARSET.*|charset.*", "", sql, re.IGNORECASE)
        sql = sql.replace("%s", "?")
        return sql