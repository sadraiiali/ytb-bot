from src.database.cursor import Cursor
    
class FakeMySQL:
    @staticmethod
    def cursor() -> "Cursor":
        return Cursor()

    def commit(self):
        pass

    def close(self):
        pass

    def ping(self, reconnect):
        pass
